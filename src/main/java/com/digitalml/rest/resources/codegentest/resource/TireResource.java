package com.digitalml.rest.resources.codegentest.resource;
        	
import java.lang.IllegalArgumentException;
import java.security.AccessControlException;

import javax.servlet.http.HttpServletRequest;

import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import io.dropwizard.jersey.PATCH;
import javax.ws.rs.Path;
import javax.ws.rs.QueryParam;
import javax.ws.rs.PathParam;
import javax.ws.rs.FormParam;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.core.SecurityContext;
import org.hibernate.validator.constraints.NotEmpty;

import java.util.*;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;

import java.lang.Object;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
	
// Customer specific imports

// Service specific imports
import com.digitalml.rest.resources.codegentest.service.TireService;
	
import com.digitalml.rest.resources.codegentest.service.TireService.GetASpecificTireReturnStatusDTO;
import com.digitalml.rest.resources.codegentest.service.TireService.GetASpecificTireReturnDTO;
import com.digitalml.rest.resources.codegentest.service.TireService.GetASpecificTireInputParametersDTO;
import com.digitalml.rest.resources.codegentest.service.TireService.FindTireReturnStatusDTO;
import com.digitalml.rest.resources.codegentest.service.TireService.FindTireReturnDTO;
import com.digitalml.rest.resources.codegentest.service.TireService.FindTireInputParametersDTO;
import com.digitalml.rest.resources.codegentest.service.TireService.FindTireByManufacturerReturnStatusDTO;
import com.digitalml.rest.resources.codegentest.service.TireService.FindTireByManufacturerReturnDTO;
import com.digitalml.rest.resources.codegentest.service.TireService.FindTireByManufacturerInputParametersDTO;
import com.digitalml.rest.resources.codegentest.service.TireService.FindTireByWidthAndProfileAndRimAndLoadReturnStatusDTO;
import com.digitalml.rest.resources.codegentest.service.TireService.FindTireByWidthAndProfileAndRimAndLoadReturnDTO;
import com.digitalml.rest.resources.codegentest.service.TireService.FindTireByWidthAndProfileAndRimAndLoadInputParametersDTO;
	
import com.digitalml.rest.resources.codegentest.*;
	
	/**
	 * Service: Tire
	 * Find tire information.8
	 *
	 * @author admin
	 * @version 1.0
	 *
	 */
	

	@Path("/")
	@Produces({ MediaType.TEXT_PLAIN, MediaType.APPLICATION_JSON })
	public class TireResource {
		
	private static final Logger LOGGER = LoggerFactory.getLogger(TireResource.class);
	
	@Context
	private SecurityContext securityContext;

	@Context
	private javax.ws.rs.core.Request request;

	@Context
	private HttpServletRequest httpRequest;

	private TireService delegateService;

	private String implementationClass = "com.digitalml.rest.resources.codegentest.service.Tire.TireServiceDefaultImpl";

	public void setImplementationClass(String className) {
		implementationClass = className;
	}

	public void setImplementationClass(Class clazz) {
		if (clazz == null)
			throw new IllegalArgumentException("Parameter clazz cannot be null");

		implementationClass = clazz.getName();
	}
		
	private TireService getCurrentImplementation() {

		Object object = null;
		try {
			Class c = Class.forName(implementationClass, true, Thread.currentThread().getContextClassLoader());
			object = c.newInstance();

		} catch (ClassNotFoundException exc) {
			LOGGER.error(implementationClass + " not found");
			return null;

		} catch (IllegalAccessException exc) {
			LOGGER.error("Cannot access " + implementationClass);
			return null;

		} catch (InstantiationException exc) {
			LOGGER.error("Cannot instantiate " + implementationClass);
			return null;
		}

		if (!(object instanceof TireService)) {
			LOGGER.error(implementationClass + " is not an instance of " + TireService.class.getName());
			return null;
		}

		return (TireService)object;
	}
	
	{
		delegateService = getCurrentImplementation();
	}
	
	public void setSecurityContext(SecurityContext securityContext) {
		this.securityContext = securityContext;
	}


	/**
	Method: getaspecifictire
		Gets tire details

	Non-functional requirements:
	*/
	
	@GET
	@Path("/tire/{id}")
	public javax.ws.rs.core.Response getaspecifictire(
		@PathParam("id")@NotEmpty String id) {

		GetASpecificTireInputParametersDTO inputs = new TireService.GetASpecificTireInputParametersDTO();
		// Prepare and check all input parameters

		inputs.setId(id);
	
		try {
			GetASpecificTireReturnDTO returnValue = delegateService.getaspecifictire(securityContext, inputs);

			ObjectMapper mapper = new ObjectMapper();
			mapper.disable(SerializationFeature.FAIL_ON_EMPTY_BEANS);

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.ok(mapper.writeValueAsString(returnValue));
			return builder.build();
			

		} catch (IllegalArgumentException e) {

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(417).entity(e.getMessage());
			return builder.build();

		} catch (AccessControlException e) {

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(403).entity(e.getMessage());
			return builder.build();
		} catch (JsonProcessingException e) {
			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(500).entity(e.getMessage());
			return builder.build();
		} finally {
		}
	}
	
	/**
	Method: findtire
		Gets a collection of tire details filtered 

	Non-functional requirements:
	*/
	
	@GET
	@Path("/tire")
	public javax.ws.rs.core.Response findtire(
		@QueryParam("offset") Integer offset,
		@QueryParam("pagesize") Integer pagesize) {

		FindTireInputParametersDTO inputs = new TireService.FindTireInputParametersDTO();
		// Prepare and check all input parameters

		inputs.setOffset(offset);
		inputs.setPagesize(pagesize);
	
		try {
			FindTireReturnDTO returnValue = delegateService.findtire(securityContext, inputs);

			ObjectMapper mapper = new ObjectMapper();
			mapper.disable(SerializationFeature.FAIL_ON_EMPTY_BEANS);

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.ok(mapper.writeValueAsString(returnValue));
			return builder.build();
			

		} catch (IllegalArgumentException e) {

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(417).entity(e.getMessage());
			return builder.build();

		} catch (AccessControlException e) {

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(403).entity(e.getMessage());
			return builder.build();
		} catch (JsonProcessingException e) {
			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(500).entity(e.getMessage());
			return builder.build();
		} finally {
		}
	}
	
	/**
	Method: findtirebyManufacturer
		Gets a collection of tire details filtered by Manufacturer

	Non-functional requirements:
	*/
	
	@GET
	@Path("/tire/find/manufacturer")
	public javax.ws.rs.core.Response findtirebyManufacturer(
		@QueryParam("Manufacturer") String Manufacturer,
		@QueryParam("offset") Integer offset,
		@QueryParam("pagesize") Integer pagesize) {

		FindTireByManufacturerInputParametersDTO inputs = new TireService.FindTireByManufacturerInputParametersDTO();
		// Prepare and check all input parameters

		inputs.setManufacturer(Manufacturer);
		inputs.setOffset(offset);
		inputs.setPagesize(pagesize);
	
		try {
			FindTireByManufacturerReturnDTO returnValue = delegateService.findtirebyManufacturer(securityContext, inputs);

			ObjectMapper mapper = new ObjectMapper();
			mapper.disable(SerializationFeature.FAIL_ON_EMPTY_BEANS);

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.ok(mapper.writeValueAsString(returnValue));
			return builder.build();
			

		} catch (IllegalArgumentException e) {

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(417).entity(e.getMessage());
			return builder.build();

		} catch (AccessControlException e) {

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(403).entity(e.getMessage());
			return builder.build();
		} catch (JsonProcessingException e) {
			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(500).entity(e.getMessage());
			return builder.build();
		} finally {
		}
	}
	
	/**
	Method: findtirebyWidthandProfileandRimandLoad
		Gets a collection of tire details filtered by Width and Profile and Rim and Load

	Non-functional requirements:
	*/
	
	@GET
	@Path("/tire/find/details")
	public javax.ws.rs.core.Response findtirebyWidthandProfileandRimandLoad(
		@QueryParam("Width") String Width,
		@QueryParam("Profile") String Profile,
		@QueryParam("Rim") String Rim,
		@QueryParam("Load") String Load,
		@QueryParam("offset") Integer offset,
		@QueryParam("pagesize") Integer pagesize) {

		FindTireByWidthAndProfileAndRimAndLoadInputParametersDTO inputs = new TireService.FindTireByWidthAndProfileAndRimAndLoadInputParametersDTO();
		// Prepare and check all input parameters

		inputs.setWidth(Width);
		inputs.setProfile(Profile);
		inputs.setRim(Rim);
		inputs.setLoad(Load);
		inputs.setOffset(offset);
		inputs.setPagesize(pagesize);
	
		try {
			FindTireByWidthAndProfileAndRimAndLoadReturnDTO returnValue = delegateService.findtirebyWidthandProfileandRimandLoad(securityContext, inputs);

			ObjectMapper mapper = new ObjectMapper();
			mapper.disable(SerializationFeature.FAIL_ON_EMPTY_BEANS);

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.ok(mapper.writeValueAsString(returnValue));
			return builder.build();
			

		} catch (IllegalArgumentException e) {

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(417).entity(e.getMessage());
			return builder.build();

		} catch (AccessControlException e) {

			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(403).entity(e.getMessage());
			return builder.build();
		} catch (JsonProcessingException e) {
			javax.ws.rs.core.Response.ResponseBuilder builder = javax.ws.rs.core.Response.status(500).entity(e.getMessage());
			return builder.build();
		} finally {
		}
	}
	
}
package com.digitalml.rest.resources.codegentest;
	
import java.util.ArrayList;
import java.util.List;
import java.util.Date;

import javax.validation.constraints.*;

/*
JSON Representation for Response:
{
  "required": [
    "tire"
  ],
  "type": "object",
  "properties": {
    "tire": {
      "$ref": "Tire"
    }
  }
}
*/

public class Response {

	@Size(max=1)
	@NotNull
	private Tire tire;

	{
		initialiseDTO();
	}

	private void initialiseDTO() {
	    tire = null;
	}
	public Tire getTire() {
		return tire;
	}
	
	public void setTire(Tire tire) {
		this.tire = tire;
	}
}
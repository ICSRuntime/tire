package com.digitalml.rest.resources.codegentest.resource;

import java.security.Principal;

import org.apache.commons.collections.CollectionUtils;

import org.junit.Assert;
import org.junit.Test;

import javax.ws.rs.core.Response;
import javax.ws.rs.core.SecurityContext;

public class TireTests {

	@Test
	public void testResourceInitialisation() {
		TireResource resource = new TireResource();
		Assert.assertNotNull(resource);
	}

	@Test
	public void testOperationGetASpecificTireNoSecurity() {
		TireResource resource = new TireResource();
		resource.setSecurityContext(null);

		Response response = resource.getaspecifictire(org.apache.commons.lang3.StringUtils.EMPTY);
		Assert.assertEquals(403, response.getStatus());
	}


	@Test
	public void testOperationFindTireNoSecurity() {
		TireResource resource = new TireResource();
		resource.setSecurityContext(null);

		Response response = resource.findtire(0, 50);
		Assert.assertEquals(403, response.getStatus());
	}


	@Test
	public void testOperationFindTireByManufacturerNoSecurity() {
		TireResource resource = new TireResource();
		resource.setSecurityContext(null);

		Response response = resource.findtirebyManufacturer(null, 0, 50);
		Assert.assertEquals(403, response.getStatus());
	}


	@Test
	public void testOperationFindTireByWidthAndProfileAndRimAndLoadNoSecurity() {
		TireResource resource = new TireResource();
		resource.setSecurityContext(null);

		Response response = resource.findtirebyWidthandProfileandRimandLoad(null, null, null, null, 0, 50);
		Assert.assertEquals(403, response.getStatus());
	}


	private SecurityContext unautheticatedSecurityContext = new SecurityContext() {

		@Override
		public boolean isUserInRole(String arg0) {
			return false;
		}

		@Override
		public boolean isSecure() {
			return false;
		}

		@Override
		public Principal getUserPrincipal() {
			return null;
		}

		@Override
		public String getAuthenticationScheme() {
			return null;
		}
	};

	private SecurityContext fullyAutheticatedSecurityContext = new SecurityContext() {

		@Override
		public boolean isUserInRole(String arg0) {
			return true;
		}

		@Override
		public boolean isSecure() {
			return false;
		}

		@Override
		public Principal getUserPrincipal() {
			return null;
		}

		@Override
		public String getAuthenticationScheme() {
			return null;
		}
	};

}
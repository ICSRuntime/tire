package com.digitalml.rest.resources.codegentest.service;

import java.security.Principal;

import static org.junit.Assert.assertNotNull;

import org.apache.commons.lang3.StringUtils;
import org.junit.Test;


import com.digitalml.rest.resources.codegentest.service.Tire.TireServiceDefaultImpl;
import com.digitalml.rest.resources.codegentest.service.TireService.GetASpecificTireInputParametersDTO;
import com.digitalml.rest.resources.codegentest.service.TireService.GetASpecificTireReturnDTO;
import javax.ws.rs.core.SecurityContext;

public class GetASpecificTireTests {

	@Test
	public void testOperationGetASpecificTireBasicMapping()  {
		TireServiceDefaultImpl serviceDefaultImpl = new TireServiceDefaultImpl();
		GetASpecificTireInputParametersDTO inputs = new GetASpecificTireInputParametersDTO();
		inputs.setId(org.apache.commons.lang3.StringUtils.EMPTY);
		GetASpecificTireReturnDTO returnValue = serviceDefaultImpl.getaspecifictire(fullyAutheticatedSecurityContext, inputs);
		
		assertNotNull(returnValue);
		assertNotNull(returnValue.getResponse());				
	}
	

	private SecurityContext fullyAutheticatedSecurityContext = new SecurityContext() {

		@Override
		public boolean isUserInRole(String arg0) {
			return true;
		}

		@Override
		public boolean isSecure() {
			return false;
		}

		@Override
		public Principal getUserPrincipal() {
			return null;
		}

		@Override
		public String getAuthenticationScheme() {
			return null;
		}
	};
}
package com.digitalml.rest.resources.codegentest.service;

import java.security.Principal;

import static org.junit.Assert.assertNotNull;

import org.apache.commons.lang3.StringUtils;
import org.junit.Test;


import com.digitalml.rest.resources.codegentest.service.Tire.TireServiceDefaultImpl;
import com.digitalml.rest.resources.codegentest.service.TireService.FindTireByWidthAndProfileAndRimAndLoadInputParametersDTO;
import com.digitalml.rest.resources.codegentest.service.TireService.FindTireByWidthAndProfileAndRimAndLoadReturnDTO;
import javax.ws.rs.core.SecurityContext;

public class FindTireByWidthAndProfileAndRimAndLoadTests {

	@Test
	public void testOperationFindTireByWidthAndProfileAndRimAndLoadBasicMapping()  {
		TireServiceDefaultImpl serviceDefaultImpl = new TireServiceDefaultImpl();
		FindTireByWidthAndProfileAndRimAndLoadInputParametersDTO inputs = new FindTireByWidthAndProfileAndRimAndLoadInputParametersDTO();
		inputs.setWidth(null);
		inputs.setProfile(null);
		inputs.setRim(null);
		inputs.setLoad(null);
		inputs.setOffset(0);
		inputs.setPagesize(50);
		FindTireByWidthAndProfileAndRimAndLoadReturnDTO returnValue = serviceDefaultImpl.findtirebyWidthandProfileandRimandLoad(fullyAutheticatedSecurityContext, inputs);
		
		assertNotNull(returnValue);
		assertNotNull(returnValue.getResponse());				
	}
	

	private SecurityContext fullyAutheticatedSecurityContext = new SecurityContext() {

		@Override
		public boolean isUserInRole(String arg0) {
			return true;
		}

		@Override
		public boolean isSecure() {
			return false;
		}

		@Override
		public Principal getUserPrincipal() {
			return null;
		}

		@Override
		public String getAuthenticationScheme() {
			return null;
		}
	};
}
package com.digitalml.rest.resources.codegentest.service;

import java.security.Principal;

import static org.junit.Assert.assertNotNull;

import org.apache.commons.lang3.StringUtils;
import org.junit.Test;


import com.digitalml.rest.resources.codegentest.service.Tire.TireServiceDefaultImpl;
import com.digitalml.rest.resources.codegentest.service.TireService.FindTireByManufacturerInputParametersDTO;
import com.digitalml.rest.resources.codegentest.service.TireService.FindTireByManufacturerReturnDTO;
import javax.ws.rs.core.SecurityContext;

public class FindTireByManufacturerTests {

	@Test
	public void testOperationFindTireByManufacturerBasicMapping()  {
		TireServiceDefaultImpl serviceDefaultImpl = new TireServiceDefaultImpl();
		FindTireByManufacturerInputParametersDTO inputs = new FindTireByManufacturerInputParametersDTO();
		inputs.setManufacturer(null);
		inputs.setOffset(0);
		inputs.setPagesize(50);
		FindTireByManufacturerReturnDTO returnValue = serviceDefaultImpl.findtirebyManufacturer(fullyAutheticatedSecurityContext, inputs);
		
		assertNotNull(returnValue);
		assertNotNull(returnValue.getResponse());				
	}
	

	private SecurityContext fullyAutheticatedSecurityContext = new SecurityContext() {

		@Override
		public boolean isUserInRole(String arg0) {
			return true;
		}

		@Override
		public boolean isSecure() {
			return false;
		}

		@Override
		public Principal getUserPrincipal() {
			return null;
		}

		@Override
		public String getAuthenticationScheme() {
			return null;
		}
	};
}